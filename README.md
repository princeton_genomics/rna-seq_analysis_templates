# RNA-Seq Analysis Templates

This analysis is based on the methodology outlined in https://sites.google.com/site/princetonhtseq/tutorials/rna-seq.


## Setup

1. Copy the contents of this repository to a new directory and create a new git repository (`git init`).



## Run Analysis

1. Customize analysis parameters as indicated below.

2. Execute `run_analysis.sh` to run analysis in a vanilla R environment and generate HTML output.

3. Once satified with analysis, commit changes to git.

4. Execute `package_results.sh` to run analysis and package results into an `tar.gz` file named with the directory, date, and latest git commit id.



## Required Customizations

### Sample QC

1. Once the data has been mapped and counts generated using the workflow on
Galaxy, download the `*_Gene_counts.txt` files to the `data` subdirectory.

2. Run the `rename_galaxy_files.sh` script to cleanup the file names.

3. Modify `sample_table.csv` to include samples and relevant experimental factors.
    
4. Modify `sample_qc.Rmd` as needed (including):

    1. Chunk `sample_heatmap`: update factor names (arguments to `paste`
    function)

    2. Chunk `sample_pca`: update factor names (`intgroup` argument of `plotPCA`
    function)

    3. Chunk `gene_body_coverage`: to include this in the report, run RSeQC Gene
    Body Coverage (BAM) tool on Galaxy, download the "R Script" output into the
    `data` directory, and uncomment this chunk

    4. Update date and author info
    
### Standard Analysis

TBD

### Non-replicate Analysis

1. Modify `deseq2_rlog_non-replicate_analysis.Rmd`

    1. Chunk `select_samples`: set `dds` to `ddsFull` (all samples) or a subset
    as needed.

    2. Chunk `droplevels`: uncomment lines and ensure there is a line to drop
    levels for any attributes of the dataset.

    3. Chunk `setup_design`: set `contrast[1]` to the appropriate factor name
    and `contrast[2:3]` to the values of that factor to be compared.

    4. Annotation chunk: set chunk attribute `child` to the appropriate
    annotation source (orgdb, biomart, or file)

### Annotation

1. Modify `annotate_results_orgdb.Rmd`

    1. Chunk `annotate_via_orgdb`: change `require(org.Hs.eg.db)` to the database
    for the target organism. **Note**: You man need to install the required
    database first (e.g.):
    ```
        ## try http:// if https:// URLs are not supported
        source("https://bioconductor.org/biocLite.R")
        biocLite("org.Mm.eg.db")
    ```

    2. Chunk  `annotate_via_orgdb`: change the first argument to `mapIds` to
    the specific organsim database imported above (e.g. `org.Mm.eg.db`).

    3. Chunk  `annotate_via_orgdb`: change the `key_field` to the appropriate
    identifier type used when generating the gene counts. Update the `cols` to
    include the "other" identifier types to be annotated.