---
title: "Sample QC"
author: "Lance Parsons <lparsons@princeton.edu>"
date: "May 11, 2015"
output: html_document
---
```{r child='init_session.Rmd'}
```

```{r child='load_data.Rmd'}
```


## Normalize the samples
Each column is divided by the geometric means of the rows. The median of these ratios (skipping the genes with a geometric mean of zero) is used as the size factor for this column.
```{r normalize_samples}
ddsFull <- estimateSizeFactors(ddsFull)
ddsFull@colData
```

## Regularized Log Transformation
Transform the data to log space using DESeq2's "rlog transformation" which 
stands for regularized log.  It transforms the average of the genes across 
samples to a log2 scale but "pulls in" those genes for which the evidence 
for strong fold changes is weak due to low counts. It does this by 
transforming the original count data to the $log_2$ scale by fitting a model
with a term for each sample and a prior distribution on the coefficients which
is estimated from the data. 
```{r rlog_transformation}
rld <- rlogTransformation(ddsFull, blind = TRUE)
```

## Sample Clustering
One use of the transformed data is sample clustering. Here, we apply the
dist function to the transpose of the transformed count matrix to get
sample-to-sample distances. We could alternatively use the variance stabilized
transformation here.
```{r sample_clustering}
distsRL <- dist(t(assay(rld)))
mat <- as.matrix(distsRL)
```

A heatmap of this distance matrix gives us an overview over similarities and dissimilarities between samples.
```{r sample_heatmap}
rownames(mat) <- colnames(mat) <- with(colData(ddsFull),
                                       paste(genotype, replicate, sep=" : "))
hmcol <- colorRampPalette(brewer.pal(9, "Blues"))(100)
heatmap.2(mat, trace="none", col = rev(hmcol), margin=c(13, 13))
```

Related to the distance matrix above, a PCA plot shows the samples in the 2D plane spanned by their first two principal components. This type of plot is useful for visualizing the overall effect of experimental covariates and batch effects.
```{r sample_pca}
print(plotPCA(rld, intgroup = c("genotype")))
```

# 5' 3' Bias
Output from RSeQC tool, 'Gene Body Coverage' (run on Galaxy)
```{r gene_body_covearage,fig.width=8,fig.height=8}
# Uncomment these lines after downloading the R Script output from the
# "Gene body coverage" RSeQC tool on Galaxy
# gene_body_coverage_script_filename <- Sys.glob(file.path(data_dir, '*R_Script*.txt'))
# gene_body_coverage_script <- system2("sed", sprintf(" -E '/^pdf|^dev\\.off/ s/^/#/' \"%s\"", gene_body_coverage_script_filename), stdout=TRUE)
# eval(parse(text=gene_body_coverage_script))
```

```{r sessionInfo}
sessionInfo()
```
